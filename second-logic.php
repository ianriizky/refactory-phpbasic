<?php

$person = [
    'name' => 'ian',
    'balance' => 10000,
];

$friedRicePrice = 15000;

function format_rupiah($value) {
    return 'Rp' . number_format($value, 0, ',', '.');
}

function calculate_balance($balance, $value) {
    if ($balance > $value) {
        return 'lebih dari';
    }

    if ($balance == $value) {
        return 'sama dengan';
    }

    return 'kurang dari';
}
