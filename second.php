<?php include_once 'second-logic.php' ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Refactory Course: Study Case Kedua</title>
</head>

<body>
    <p><?= ucfirst($person['name']) ?> mau beli nasi goreng</p>

    <p>Harga <?= $quantity = 1 ?> bungkus nasi goreng <?= format_rupiah($friedRicePrice) ?></p>

    <p>Uang <?= $person['name'] ?> hanya <?= format_rupiah($person['balance']) ?></p>

    <p>Maka uang <?= $person['name'] ?> <strong><?= calculate_balance($person['balance'], $friedRicePrice) ?></strong> harga nasi goreng</p>

    <p><a href="/"><<< Go Back Home</a></p>
</body>

</html>
